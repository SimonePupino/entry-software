#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//                                     //
// ---------- Worker Struct ---------- //
//                                     //

struct worker { 
    int entryGate;
    int serialNumber;
    char entryTime[8];
    char exitTime[8];
};

struct worker workersDB[1000]; // We assume that there is a maximum of 1000 workers

int main(int argc, char *argv[]) {
    //                                          //
    // ---------- Preliminary checks ---------- //
    //                                          //
    
    if (argc != 3) { // Checking number of arguments provided
        if (argc < 3) printf("Too few arguments provided.\n");
        else printf("Too many arguments provided.\n"); 
        return 0;
    }
    size_t modeLength = strlen(argv[2]);
    if (modeLength != 2) { // Checking dimension of mode parameter
        printf("Illegal mode length dimension.\n");
        return 0;
    }
    if ((strcmp(argv[2],"-v\n")) == 0) printf("\ncomparison result to -v: %i\n", strcmp(argv[2],"-v"));
    if ((strcmp(argv[2],"-c\n")) == 0) printf("\ncomparison result to -c: %i\n", strcmp(argv[2],"-c"));
    
    /* TODO: Need to check path and filename */
    
    size_t pathLength = strlen(argv[0]); // Length of path, we need that for filename malloc
    char * filename = malloc(pathLength+1);
    char * mode = malloc(3); // We assume that mode can be only "-v" or "-c", so 3 character is enough
    strcpy(filename, argv[1]); 
    strcpy(mode, argv[2]);
    
    printf("First parameter: %s \nSecond parameter: %s\n", filename, mode );

    free(filename);
    free(mode);

    return 0;
}